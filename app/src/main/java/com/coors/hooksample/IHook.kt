package com.coors.hooksample

import de.robv.android.xposed.callbacks.XC_LoadPackage

interface IHook {
    @Throws(Throwable::class)
    fun handleLoadPackage(lpparam: XC_LoadPackage.LoadPackageParam)
}