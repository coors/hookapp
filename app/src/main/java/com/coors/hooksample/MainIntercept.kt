package com.coors.hooksample

import de.robv.android.xposed.IXposedHookLoadPackage
import de.robv.android.xposed.XposedBridge
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam

class MainIntercept : IXposedHookLoadPackage {
    //    private ICracker mCracker;
    @Throws(Throwable::class)
    override fun handleLoadPackage(lpparam: LoadPackageParam) {
        XposedBridge.log("do hook")
        if (lpparam.packageName == XPConstant.THIS_PACKAGE) {
            XposedBridge.log("find package")
            TestHook().handleLoadPackage(lpparam)
        }
    }

}