package com.coors.hooksample

import android.util.Log
import com.coors.hooksample.XPConstant.Companion.MAIN_ACTIVITY
import de.robv.android.xposed.XC_MethodHook
import de.robv.android.xposed.XposedBridge
import de.robv.android.xposed.XposedHelpers
import de.robv.android.xposed.callbacks.XC_LoadPackage

class TestHook : IHook {
    companion object {
    }

    override fun handleLoadPackage(lpparam: XC_LoadPackage.LoadPackageParam) {
        XposedBridge.log(
            "handleLoadPackage: " + lpparam.classLoader + ", param: " + lpparam
        )

        XposedBridge.log(
            String.format(
                "package: %s, process: %s",
                lpparam.packageName,
                lpparam.processName
            )
        )

        XposedHelpers.findAndHookMethod(
            MAIN_ACTIVITY,
            lpparam.classLoader,
            "isHooked",
            object : XC_MethodHook() {
                override fun afterHookedMethod(param: MethodHookParam?) {
                    super.afterHookedMethod(param)
                    XposedBridge.log("hook finish")
                    param?.result = true
                }
            })

    }


}