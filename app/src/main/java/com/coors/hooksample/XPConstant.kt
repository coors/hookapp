package com.coors.hooksample

class XPConstant {
    companion object {
        // alipay package and class name
        const val ALI_PACKAGE = "com.eg.android.AlipayGphone"
        const val APPLICATION_CLASS = "com.alipay.mobile.quinox.LauncherApplication"
        const val MONEY_RPC_CLASS = "com.alipay.transferprod.rpc.CollectMoneyRpc"
        const val ZXING_HELPER_CLASS = "com.alipay.android.phone.wallet.ZXingHelper"
        const val BARCODE_FORMAT_ENUM =
            "com.alipay.android.phone.wallet.minizxing.BarcodeFormat"
        const val ERROR_CORRECTION_ENUM =
            "com.alipay.android.phone.wallet.minizxing.ErrorCorrectionLevel"
        const val CONTEXT_IMPL_CLASS =
            "com.alipay.mobile.core.impl.MicroApplicationContextImpl"
        const val RPC_SERVICE_CLASS =
            "com.alipay.mobile.framework.service.common.RpcService"
        const val LAUNCHER_AGENT_CLASS =
            "com.alipay.mobile.framework.LauncherApplicationAgent"


        // Test
        const val THIS_PACKAGE = "com.coors.hooksample"
        const val MAIN_ACTIVITY = "${THIS_PACKAGE}.MainActivity"
    }
}